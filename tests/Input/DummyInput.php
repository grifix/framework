<?php
declare(strict_types=1);

namespace Grifix\Framework\Tests\Input;

use Grifix\Framework\Ui\Input\AbstractInput;
use Grifix\Framework\Ui\Input\Types\BoolInputType;
use Grifix\Framework\Ui\Input\Types\FloatInputType;
use Grifix\Framework\Ui\Input\Types\IntInputType;
use Grifix\Framework\Ui\Input\Types\MoneyInputType;
use Grifix\Framework\Ui\Input\Types\StrictBoolInputType;
use Grifix\Framework\Ui\Input\Types\StrictIntInputType;
use Grifix\Framework\Ui\Input\Types\StringInputType;
use Grifix\Framework\Ui\Input\Types\UuidInputType;
use Grifix\Money\Money\Money;
use Grifix\Uuid\Uuid;
use Symfony\Component\Validator\Constraints\Collection;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Optional;
use Symfony\Component\Validator\Constraints\Required;

final class DummyInput extends AbstractInput
{

    public function getId(): Uuid
    {
        return $this->getValue('id', UuidInputType::class)->toUuid();
    }

    public function getName(): string
    {
        return $this->getValue('name', StringInputType::class)->toString();
    }

    public function getPrice(): ?Money
    {
        return $this->getValueOrNull('price', MoneyInputType::class)?->toMoney();
    }

    public function getNumber(): ?int
    {
        return $this->getValueOrNull('number', StrictIntInputType::class)?->toInt();
    }

    public function getSize(): ?float
    {
        return $this->getValueOrNull('size', FloatInputType::class)?->toFloat();
    }

    public function getEnabled(): ?bool
    {
        return $this->getValueOrNull('enabled', BoolInputType::class)?->toBool();
    }

    public static function createConstraint(): Collection
    {
        return new Collection(
            [
                'allowExtraFields' => false,
                'fields' => [
                    'id' => new Optional(UuidInputType::createConstraint()),
                    'name' => new Required([
                            new NotBlank(),
                            StringInputType::createConstraint()
                        ]
                    ),
                    'price' => new Optional(MoneyInputType::createConstraint()),
                    'number' => new Optional(StrictIntInputType::createConstraint()),
                    'size' => new Optional(FloatInputType::createConstraint()),
                    'enabled' => new Optional(BoolInputType::createConstraint()),
                    'count' => new Optional(IntInputType::createConstraint()),
                    'isActive' => new Optional(StrictBoolInputType::createConstraint()),
                ],
            ]
        );
    }
}
