<?php
declare(strict_types=1);

namespace Grifix\Framework\Tests\Input;

use Exception;
use Grifix\Framework\Ui\Input\InputFactory;
use Grifix\Framework\Ui\Input\InvalidInputException;
use Grifix\Money\Money\Money;
use Grifix\Uuid\Uuid;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Validator\ValidatorBuilder;

final class InputTest extends TestCase
{
    private InputFactory $inputFactory;

    protected function setUp(): void
    {
        $this->inputFactory = new InputFactory((new ValidatorBuilder())->getValidator());
    }

    public function testItCreates(): void
    {
        $data = [
            'id' => '5e8d3901-bd23-44d4-9903-07da6b22ca13',
            'name' => 'Joe',
            'price' => [
                'amount' => 100,
                'currency' => 'USD'
            ],
            'size' => 1.5,
            'number' => 8,
            'enabled' => true
        ];

        $input = $this->inputFactory->createInput(DummyInput::class, $data);
        self::assertEquals(Uuid::createFromString($data['id']), $input->getId());
        self::assertEquals($data['name'], $input->getName());
        self::assertEquals(
            Money::withCurrency($data['price']['amount'], $data['price']['currency']),
            $input->getPrice(),
        );
        self::assertEquals($data['size'], $input->getSize());
        self::assertEquals($data['number'], $input->getNumber());
        self::assertEquals($data['enabled'], $input->getEnabled());
    }

    /**
     *
     * @param mixed[] $data
     * @param array{array{message:string, path:string}} $expectedErrors
     * @dataProvider itFailsToCreateDataProvider
     */
    public function testItFailsToCreate(array $data, array $expectedErrors): void
    {
        try {
            $this->inputFactory->createInput(DummyInput::class, $data);
        } catch (Exception $e) {
            self::assertInstanceOf(InvalidInputException::class, $e);
            foreach ($expectedErrors as $i => $error) {
                self::assertEquals($error['message'], $e->violations->get($i)->getMessage());
                self::assertEquals($error['path'], $e->violations->get($i)->getPropertyPath());
            }
        }

    }

    /**
     * @return mixed[]
     */
    public function itFailsToCreateDataProvider(): array
    {
        return [
            'empty data' => [
                [],
                [
                    [
                        'message' => 'This field is missing.',
                        'path' => '[name]'
                    ],
                ]
            ],
            'price without currency' => [
                [
                    'id' => 'a77a9000-fb24-4332-9225-5acba77b656f',
                    'name' => 'Joe',
                    'price' => [
                        'amount' => 100
                    ]
                ],
                [
                    [
                        'message' => 'This field is missing.',
                        'path' => '[price][currency]'
                    ]
                ],
            ],
            'price without amount' => [
                [
                    'id' => 'a77a9000-fb24-4332-9225-5acba77b656f',
                    'name' => 'Joe',
                    'price' => [
                        'currency' => 'USD'
                    ]
                ],
                [
                    [
                        'message' => 'This field is missing.',
                        'path' => '[price][amount]'
                    ]
                ],
            ],
            'invalid currency' => [
                [
                    'id' => 'a77a9000-fb24-4332-9225-5acba77b656f',
                    'name' => 'Joe',
                    'price' => [
                        'amount' => 100,
                        'currency' => 'XXX'
                    ]
                ],
                [
                    [
                        'message' => 'This value is not a valid currency.',
                        'path' => '[price][currency]'
                    ]
                ],
            ],
            'invalid bool' => [
                [
                    'name' => 'test',
                    'enabled' => 'true2'
                ],
                [
                    [
                        'message' => 'This value should satisfy at least one of the following constraints: [1] This value should be of type bool. [2] This value should match the pattern: ^(1|0|true|false|TRUE|FALSE)$.',
                        'path' => '[enabled]'
                    ]
                ]
            ],
            'invalid strict bool' => [
                [
                    'name' => 'test',
                    'isActive' => 'true'
                ],
                [
                    [
                        'message' => 'This value should be of type bool.',
                        'path' => '[isActive]'
                    ]
                ]
            ],
            'invalid strict int' => [
                [
                    'name' => 'test',
                    'number' => '2'
                ],
                [
                    [
                        'message' => 'This value should be of type int.',
                        'path' => '[number]'
                    ]
                ]
            ],
            'invalid int' => [
                [
                    'name' => 'test',
                    'count' => '2a'
                ],
                [
                    [
                        'message' => 'This value should satisfy at least one of the following constraints: [1] This value should be of type int. [2] This value should match the pattern: ^[0-9]*$.',
                        'path' => '[count]'
                    ]
                ]
            ],
            'invalid float' => [
                [
                    'name' => 'test',
                    'size' => '2.13'
                ],
                [
                    [
                        'message' => 'This value should be of type float.',
                        'path' => '[size]'
                    ]
                ]
            ]
        ];
    }
}
