<?php

declare(strict_types=1);

namespace PHPSTORM_META{

    use Grifix\Framework\Application\QueryBusInterface;

    override(
        QueryBusInterface::ask(0),
        map([
            '' => '@Result'
        ])
    );
}
