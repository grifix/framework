<?php

declare(strict_types=1);

namespace Grifix\Framework\Application;

interface QueryBusInterface
{
    /**
     * to avoid phpstan undefined property error in client code
     * @return \stdClass
     */
    public function ask(object $query):object;
}
