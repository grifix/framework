<?php
declare(strict_types=1);

namespace Grifix\Framework\Application;

use Grifix\Uuid\Uuid;

interface MessageBusInterface
{
    public function sendMessage(object $message, string $channelId, Uuid $sequenceId): void;
}
