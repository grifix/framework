<?php

declare(strict_types=1);

namespace Grifix\Framework\Application;

interface CommandBusInterface
{
    public function tell(object $command): void;
}
