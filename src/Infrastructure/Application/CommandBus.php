<?php

declare(strict_types=1);

namespace Grifix\Framework\Infrastructure\Application;

use Doctrine\DBAL\Connection;
use Grifix\EntityManager\EntityManagerInterface;
use Grifix\EventStore\EventStoreInterface;
use Grifix\Framework\Application\CommandBusInterface;
use Symfony\Component\Messenger\Exception\HandlerFailedException;
use Symfony\Component\Messenger\MessageBusInterface;

final class CommandBus implements CommandBusInterface
{
    public function __construct(
        private readonly EventStoreInterface $eventStore,
        private readonly MessageBusInterface $messageBus,
        private readonly Connection $connection,
        private readonly EntityManagerInterface $entityManager
    ) {
    }

    public function tell(object $command): void
    {
        try {
            $this->messageBus->dispatch($command);
        } catch (HandlerFailedException $handlerException) {
            $exception = $handlerException;
            if ($handlerException->getPrevious()) {
                $exception = $handlerException->getPrevious();
            }
            if(str_contains($exception::class, "\\Domain\\")){
                $this->changeState();
            }
            throw $exception;
        }

        $this->changeState();
    }

    private function changeState(): void
    {
        $this->connection->beginTransaction();
        try {
            $this->entityManager->flush();
            $this->eventStore->flush();
        } catch (\Throwable $e) {
            $this->connection->rollBack();
            throw $e;
        }
        $this->connection->commit();
    }
}
