<?php
declare(strict_types=1);

namespace Grifix\Framework\Infrastructure\Application;

use Grifix\EventStore\EventStoreInterface;
use Grifix\Framework\Application\MessageBusInterface;
use Grifix\Uuid\Uuid;

final class MessageBus implements MessageBusInterface
{
    public function __construct(private readonly EventStoreInterface $eventStore)
    {
    }

    public function sendMessage(object $message, string $channelId, Uuid $sequenceId): void
    {
        $this->eventStore->storeEvent($message, $channelId, $sequenceId);
        $this->eventStore->flush();
    }
}
