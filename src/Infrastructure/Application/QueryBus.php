<?php

declare(strict_types=1);

namespace Grifix\Framework\Infrastructure\Application;

use Grifix\Framework\Application\QueryBusInterface;
use Symfony\Component\Messenger\Exception\HandlerFailedException;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Messenger\Stamp\HandledStamp;

final class QueryBus implements QueryBusInterface
{

    public function __construct(private readonly MessageBusInterface $messageBus)
    {
    }

    public function ask(object $query):object
    {

        try{
            /** @var HandledStamp $stamp */
            $stamp = $this->messageBus->dispatch($query)->last(HandledStamp::class);
        }
        catch(HandlerFailedException $exception){
            if($exception->getPrevious()){
                throw $exception->getPrevious();
            }
            throw $exception;
        }
        /** @var \stdClass $result */
        $result = $stamp->getResult();
        return $result;
    }
}
