<?php
declare(strict_types=1);

namespace Grifix\Framework\Infrastructure;

use Grifix\EntityManager\EntityManagerInterface;
use Grifix\EntityManager\EntityRepository\Exceptions\EntityDoesNotExistException;
use Grifix\Reflection\ReflectionObject;
use Grifix\Uuid\Uuid;

abstract class AbstractRepository
{

    public function __construct(private readonly EntityManagerInterface $entityManager)
    {
    }

    protected function doAdd(object $entity, string $idProperty = 'id'): void
    {
        $reflection = new ReflectionObject($entity);
        /** @var Uuid $id */
        $id = $reflection->getPropertyValue($idProperty);
        $this->entityManager->add($entity, $id->toString());
    }

    /**
     * @template T
     * @param class-string<T> $entityClass
     * @return T
     * @throws \Throwable
     */
    protected function doGet(Uuid $id, string $exceptionClass, string $entityClass)
    {
        try {
            /** @var T $result */
            $result = $this->entityManager->get($entityClass, $id->toString());
        } catch (EntityDoesNotExistException) {
            /** @var \Throwable $exception */
            $exception = new $exceptionClass($id);
            throw $exception;
        }
        return $result;
    }
}
