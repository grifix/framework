<?php

declare(strict_types=1);

namespace Grifix\Framework\Config;

use Symfony\Component\DependencyInjection\Loader\Configurator\ContainerConfigurator;
use Symfony\Component\DependencyInjection\Loader\Configurator\PrototypeConfigurator;
use Symfony\Component\DependencyInjection\Loader\Configurator\ServicesConfigurator;

final class ModuleServicesConfigurator
{
    private readonly ServicesConfigurator $services;

    private readonly ?string $env;

    private function __construct(
        ContainerConfigurator $containerConfigurator,
        private ?string $modulesDir = null
    ) {
        $this->env = $containerConfigurator->env();
        $this->services = $containerConfigurator->services();
        if (null === $this->modulesDir) {
            $this->modulesDir = dirname(__DIR__, 5) . DIRECTORY_SEPARATOR . 'modules';
        }
    }

    public static function create(ContainerConfigurator $containerConfigurator, ?string $modulesDir = null): self
    {
        return new self($containerConfigurator, $modulesDir);
    }

    public function configure(string $moduleName, string $moduleNamespace): void
    {
        $this->services
            ->defaults()
            ->autowire()
            ->autoconfigure();

        $this->configureRequestHandlers($moduleName, $moduleNamespace);
        $this->configureCliHandlers($moduleName, $moduleNamespace);
        $this->configureMessageHandlers($moduleName, $moduleNamespace);
        $this->configureCommandHandlers($moduleName, $moduleNamespace);
        $this->configureQueryHandlers($moduleName, $moduleNamespace);
        $this->configureFactories($moduleName, $moduleNamespace);
        $this->configureSubscribers($moduleName, $moduleNamespace);
        if ($this->env === 'test') {
            $this->configureBehatContexts($moduleName, $moduleNamespace);
        }
    }

    private function configureCliHandlers(string $module, string $namespace): void
    {
        if (!$this->namespaceExists($module, 'Ui')) {
            return;
        }
        $this->loadServices(
            $module,
            $namespace . "\\Ui",
            'Ui/*/CliHandlers/{*,*/*}*CliHandler.php'
        )->tag('console.command');
    }

    private function configureSubscribers(string $module, string $namespace): void
    {
        if (!$this->namespaceExists($module, 'Application')) {
            return;
        }
        $this->loadServices(
            $module,
            $namespace . "\\Application",
            'Application/*/Subscribers/{*,*/*}*Subscriber.php'
        )->public();
    }

    private function configureMessageHandlers(string $module, string $namespace): void
    {
        if (!$this->namespaceExists($module, 'Ui')) {
            return;
        }
        $this->loadServices(
            $module,
            $namespace . "\\Ui",
            'Ui/*/MessageHandlers/{*,*/*}*MessageHandler.php'
        )->public();
    }

    private function configureFactories(string $module, string $namespace): void
    {
        if (!$this->namespaceExists($module, 'Domain')) {
            return;
        }
        $this->loadServices(
            $module,
            $namespace . "\\Domain",
            'Domain/*/*Factory.php'
        );
    }

    private function configureBehatContexts(string $module, string $namespace): void
    {
        if (!$this->namespaceExists($module, 'Behavioral\Contexts', true)) {
            return;
        }
        $this->loadServices(
            $module,
            $namespace . '\Tests\Behavioral\Contexts',
            'Behavioral/Contexts/{*,*/*,*/*/*}*Context.php',
            true
        );
    }

    private function configureRequestHandlers(string $module, string $namespace): void
    {
        if (!$this->namespaceExists($module, 'Ui')) {
            return;
        }
        $this->loadServices(
            $module,
            $namespace . "\\Ui",
            'Ui/*/RequestHandlers/{*,*/*}*RequestHandler.php'
        )
            ->tag('controller.service_arguments')
            ->public();
    }

    private function configureCommandHandlers(string $module, string $namespace): void
    {
        if (!$this->namespaceExists($module, 'Application')) {
            return;
        }
        $this->loadServices(
            $module,
            $namespace . "\\Application",
            'Application/*/Commands/{*,*/*}*CommandHandler.php'
        )->tag('messenger.message_handler');
    }

    private function configureQueryHandlers(string $module, string $namespace): void
    {
        if (!$this->namespaceExists($module, 'Application')) {
            return;
        }
        $this->loadServices(
            $module,
            $namespace . "\\Application",
            'Application/*/Queries/{*,*/*}*QueryHandler.php'
        )->tag('messenger.message_handler');
    }

    private function loadServices(
        string $module,
        string $namespace,
        string $resource,
        bool $tests = false
    ): PrototypeConfigurator {
        $src = 'src';
        if ($tests) {
            $src = 'tests';
        }
        $namespace = 'Modules\\' . $namespace . '\\';
        $resource = '%kernel.project_dir%/modules/' . $module . '/' . $src . '/' . $resource;
        return $this->services->defaults()->autowire()->autoconfigure()->load(
            $namespace,
            $resource
        );
    }

    private function namespaceExists(string $module, string $namespace, bool $tests = false): bool
    {
        $src = 'src';
        if ($tests) {
            $src = 'tests';
        }
        $namespace = str_replace('\\', DIRECTORY_SEPARATOR, $namespace);
        return file_exists($this->modulesDir . DIRECTORY_SEPARATOR . $module . DIRECTORY_SEPARATOR . $src . DIRECTORY_SEPARATOR . $namespace);
    }
}
