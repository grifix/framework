<?php
declare(strict_types=1);

namespace Grifix\Framework\Ui;

use Grifix\ArrayWrapper\ArrayWrapper;
use Grifix\ErrorPresenter\ExceptionConverter;
use Grifix\ErrorPresenterBundle\ExceptionConverterProviderInterface;
use Grifix\Framework\Ui\Input\InvalidInputException;

final class ExceptionConverterProvider implements ExceptionConverterProviderInterface
{
    private function makePath(string $path): string
    {
        $result = str_replace('][', '.', $path);
        return 'errors.' . str_replace(['[', ']'], '', $result);
    }

    public function getConverters(): array
    {
        return [
            ExceptionConverter::create(
                InvalidInputException::class,
                400,
                errorMessage: function (InvalidInputException $exception): array {
                    $result = ArrayWrapper::create();
                    foreach ($exception->violations as $violation) {
                        $result->setElement($this->makePath($violation->getPropertyPath()), $violation->getMessage());
                    }
                    return $result->getWrapped();
                }),
        ];
    }
}
