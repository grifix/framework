<?php
declare(strict_types=1);

namespace Grifix\Framework\Ui;

use Grifix\Framework\Ui\Application\ApplicationInterface;
use Grifix\Framework\Ui\Input\InputFactory;
use Grifix\Framework\Ui\Input\InvalidInputException;
use Grifix\View\ViewFactoryInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\Constraints;
use Symfony\Component\Validator\Validator\ValidatorInterface;

abstract class AbstractRequestHandler
{
    public function __construct(
        protected readonly ApplicationInterface $application,
        protected readonly ViewFactoryInterface $viewFactory,
        protected readonly InputFactory $inputFactory,
        protected readonly ValidatorInterface $validator
    ) {
    }

    /**
     * @template T
     * @param Request $request
     * @param class-string<T> $inputClass
     * @return T
     *
     * @throws InvalidInputException
     */
    protected function createInputFromBody(Request $request, string $inputClass): mixed
    {
        $violations = $this->validator->validate(
            $request->getContent(),
            [new Constraints\Json()]
        );
        if ($violations->count() > 0) {
            throw new InvalidInputException($violations);
        }

        /** @var mixed[] $data */
        $data = json_decode(
            $request->getContent(),
            true
        );

        /** @var T $result */
        $result = $this->inputFactory->createInput(
            $inputClass,
            $data
        );
        return $result;
    }

    /**
     * @template T
     * @param Request $request
     * @param class-string<T> $inputClass
     * @return T
     *
     * @throws InvalidInputException
     */
    protected function createInputFromQueryString(Request $request, string $inputClass)
    {
        $params = [];
        $queryString = $request->getQueryString();
        if ($queryString) {
            parse_str($queryString, $params);
        }

        /** @var T $result */
        $result = $this->inputFactory->createInput($inputClass, $params);
        return $result;
    }

    /**
     * @param mixed[] $params
     */
    protected function renderView(string $viewPath, array $params = []): Response
    {
        return new Response($this->viewFactory->createView($viewPath)->render($params));
    }
}
