<?php
declare(strict_types=1);

namespace Grifix\Framework\Ui;

use Grifix\Framework\Ui\Application\ApplicationInterface;
use Symfony\Component\Console\Command\Command;

abstract class AbstractCliHandler extends Command
{
    public function __construct(protected readonly ApplicationInterface $application)
    {
        parent::__construct();
    }
}
