<?php
declare(strict_types=1);

namespace Grifix\Framework\Ui;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

final class WidgetRendererPlugin
{

    public function __construct(private readonly ContainerInterface $container)
    {
    }

    /**
     * @param mixed[] $params
     * @throws \Exception
     */
    public function renderWidget(string $actionHandler, array $params = []): string
    {
        $request = new Request($params);

        /** @var ?AbstractRequestHandler $handler */
        $handler = $this->container->get($actionHandler);
        if (null === $handler) {
            throw new \Exception(sprintf('Handler [%s] not found!', $actionHandler));
        }

        if (!method_exists($handler, '__invoke')) {
            throw new \Exception(sprintf('Handler [%s] is not invokable!', $actionHandler));
        }

        /** @var Response $response */
        $response = $handler->__invoke($request);
        $result = $response->getContent();
        if (!$result) {
            return '';
        }
        return $result;
    }
}
