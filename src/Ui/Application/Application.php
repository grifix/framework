<?php
declare(strict_types=1);

namespace Grifix\Framework\Ui\Application;

use Grifix\Framework\Application\CommandBusInterface;
use Grifix\Framework\Application\QueryBusInterface;

final class Application implements ApplicationInterface
{
    public function __construct(
        private readonly QueryBusInterface $queryBus,
        private readonly CommandBusInterface $commandBus
    ) {
    }

    public function tell(object $command): void
    {
        $this->commandBus->tell($command);
    }

    public function ask(object $query): object
    {
        return $this->queryBus->ask($query);
    }
}
