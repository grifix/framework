<?php
declare(strict_types=1);

namespace Grifix\Framework\Ui\Application;

use Grifix\Framework\Application\CommandBusInterface;
use Grifix\Framework\Application\QueryBusInterface;

interface ApplicationInterface extends QueryBusInterface, CommandBusInterface
{

}
