<?php
declare(strict_types=1);

namespace Grifix\Framework\Ui\Input;

use Symfony\Component\Validator\ConstraintViolationListInterface;

final class InvalidInputException extends \Exception
{
    public function __construct(public readonly ConstraintViolationListInterface $violations)
    {
        parent::__construct('Invalid input!');
    }
}
