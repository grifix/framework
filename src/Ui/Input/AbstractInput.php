<?php
declare(strict_types=1);

namespace Grifix\Framework\Ui\Input;

use Grifix\ArrayWrapper\ArrayWrapper;
use Grifix\Framework\Ui\Input\Exceptions\NullValueException;
use Grifix\Framework\Ui\Input\Exceptions\UnsupportedInputTypeException;
use Grifix\Framework\Ui\Input\Types\BoolInputType;
use Grifix\Framework\Ui\Input\Types\EmailInputType;
use Grifix\Framework\Ui\Input\Types\FloatInputType;
use Grifix\Framework\Ui\Input\Types\IntInputType;
use Grifix\Framework\Ui\Input\Types\IpAddressInputType;
use Grifix\Framework\Ui\Input\Types\MoneyInputType;
use Grifix\Framework\Ui\Input\Types\PositiveIntType;
use Grifix\Framework\Ui\Input\Types\StrictBoolInputType;
use Grifix\Framework\Ui\Input\Types\StrictIntInputType;
use Grifix\Framework\Ui\Input\Types\StringInputType;
use Grifix\Framework\Ui\Input\Types\UuidInputType;
use Symfony\Component\Validator\Constraints\Collection;
use Symfony\Component\Validator\Validator\ValidatorInterface;

abstract class AbstractInput
{
    private ArrayWrapper $data;

    private const ALLOWED_TYPES = [
        MoneyInputType::class,
        StringInputType::class,
        UuidInputType::class,
        BoolInputType::class,
        FloatInputType::class,
        StrictIntInputType::class,
        PositiveIntType::class,
        EmailInputType::class,
        IpAddressInputType::class
    ];

    /**
     * @param mixed[] $data
     * @throws InvalidInputException
     */
    public function __construct(
        private readonly ValidatorInterface $validator,
        array $data,
    ) {
        $this->validate($data);
        $this->data = ArrayWrapper::create($data);
    }

    /**
     * @param mixed[] $data
     * @throws InvalidInputException
     */
    protected function validate(array $data): void
    {
        $violations = $this->validator->validate($data, $this->createConstraint());
        if ($violations->count() > 0) {
            throw new InvalidInputException($violations);
        }
    }

    /**
     * @template T
     * @param class-string<T> $inputType
     * @return T|null
     * @throws UnsupportedInputTypeException
     */
    protected function getValueOrNull(string $path, string $inputType, mixed $defaultValue = null): mixed
    {
        try {
            return $this->getValue($path, $inputType, $defaultValue);
        } catch (NullValueException) {
            return null;
        }
    }

    /**
     * @template T
     * @param class-string<T> $inputType
     * @return T
     * @throws UnsupportedInputTypeException
     * @throws NullValueException
     */
    protected function getValue(string $path, string $inputType, mixed $defaultValue = null)
    {

        if (!in_array($inputType, self::ALLOWED_TYPES)) {
            throw new UnsupportedInputTypeException($inputType);
        }

        /** @var T $result */
        $result = match ($inputType) {
            StringInputType::class => $this->doGetValue(
                $path,
                function (mixed $value) {
                    /** @var string $value */
                    return new StringInputType($value);
                },
                $defaultValue
            ),
            UuidInputType::class => $this->doGetValue(
                $path,
                function (mixed $value) {
                    /** @var string $value */
                    return new UuidInputType($value);
                },
                $defaultValue
            ),
            MoneyInputType::class => $this->doGetValue(
                $path,
                function (mixed $value) {
                    /** @var array{amount:numeric-string|int, currency:string} $value */
                    return new MoneyInputType($value);
                },
                $defaultValue
            ),
            BoolInputType::class => $this->doGetValue(
                $path,
                function (mixed $value) {
                    /** @var bool $value */
                    return new BoolInputType($value);
                },
                $defaultValue
            ),
            StrictBoolInputType::class => $this->doGetValue(
                $path,
                function (mixed $value) {
                    /** @var bool $value */
                    return new StrictBoolInputType($value);
                },
                $defaultValue
            ),
            StrictIntInputType::class => $this->doGetValue(
                $path,
                function (mixed $value) {
                    /** @var int $value */
                    return new StrictIntInputType($value);
                },
                $defaultValue
            ),
            IntInputType::class => $this->doGetValue(
                $path,
                function (mixed $value) {
                    /** @var int $value */
                    return new IntInputType($value);
                },
                $defaultValue
            ),
            FloatInputType::class => $this->doGetValue(
                $path,
                function (mixed $value) {
                    /** @var float $value */
                    return new FloatInputType($value);
                },
                $defaultValue
            ),
            PositiveIntType::class => $this->doGetValue(
                $path,
                function (mixed $value) {
                    /** @var int|string $value */
                    return new PositiveIntType($value);
                },
                $defaultValue
            ),
            EmailInputType::class => $this->doGetValue(
                $path,
                function (mixed $value) {
                    /** @var string $value */
                    return new EmailInputType($value);
                },
                $defaultValue
            ),
            IpAddressInputType::class => $this->doGetValue(
                $path,
                function (mixed $value) {
                    /** @var string $value */
                    return new IpAddressInputType($value);
                },
                $defaultValue
            ),
            default => null
        };
        return $result;
    }

    private function doGetValue(string $path, callable $getValue, mixed $defaultValue = null): mixed
    {
        $value = $this->data->getElement($path, $defaultValue);
        if (null === $value) {
            throw new NullValueException($path);
        }
        return $getValue($value);
    }

    protected function hasValue(string $path): bool
    {
        return $this->data->hasElement($path);
    }

    abstract public static function createConstraint(): Collection;
}
