<?php
declare(strict_types=1);

namespace Grifix\Framework\Ui\Input;

final class Error
{
    public function __construct(
        public readonly string $property,
        public readonly string $message,
    ) {
    }
}
