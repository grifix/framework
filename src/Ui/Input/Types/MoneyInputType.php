<?php
declare(strict_types=1);

namespace Grifix\Framework\Ui\Input\Types;

use Grifix\Money\Money\Money;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\Constraints;
use Symfony\Component\Validator\Constraints\Collection;

final class MoneyInputType implements InputTypeInterface
{

    /**
     * @param array{amount:numeric-string|int, currency:string} $value
     */
    public function __construct(private readonly array $value)
    {

    }

    public static function createConstraint(): Constraint
    {
        return new Collection(
            [
                'allowExtraFields' => false,
                'fields' => [
                    'amount' => new Constraints\Required([
                            new Constraints\NotBlank(),
                            new Constraints\AtLeastOneOf([
                                new Constraints\Type('int'),
                                new Constraints\Regex(
                                    '/^[0-9]*$/',
                                    'Value is not numeric'
                                )
                            ])
                        ]
                    ),
                    'currency' => new Constraints\Required([
                        new Constraints\NotBlank(),
                        new Constraints\Currency()
                    ])
                ],
            ]
        );
    }

    public function toMoney(): Money
    {
        return Money::withCurrency(
            $this->value['amount'],
            $this->value['currency']
        );
    }
}
