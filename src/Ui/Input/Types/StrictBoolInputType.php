<?php
declare(strict_types=1);

namespace Grifix\Framework\Ui\Input\Types;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\Constraints\AtLeastOneOf;
use Symfony\Component\Validator\Constraints\Regex;
use Symfony\Component\Validator\Constraints\Type;

final class StrictBoolInputType implements InputTypeInterface
{

    public function __construct(private readonly bool $value)
    {
    }

    public static function createConstraint(): Constraint
    {
        return new Type('bool');
    }

    public function toBool(): bool
    {
        return $this->value;
    }
}
