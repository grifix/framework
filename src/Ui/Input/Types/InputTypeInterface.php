<?php
declare(strict_types=1);

namespace Grifix\Framework\Ui\Input\Types;

use Symfony\Component\Validator\Constraint;

interface InputTypeInterface
{
    public static function createConstraint(): Constraint;
}
