<?php
declare(strict_types=1);

namespace Grifix\Framework\Ui\Input\Types;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\Constraints\Type;

final class StringInputType implements InputTypeInterface
{

    public function __construct(private readonly string $value)
    {
    }

    public static function createConstraint(): Constraint
    {
        return new Type('string');
    }

    public function toString(): string
    {
        return $this->value;
    }
}
