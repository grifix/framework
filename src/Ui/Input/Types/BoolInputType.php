<?php
declare(strict_types=1);

namespace Grifix\Framework\Ui\Input\Types;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\Constraints\AtLeastOneOf;
use Symfony\Component\Validator\Constraints\Regex;
use Symfony\Component\Validator\Constraints\Type;

final class BoolInputType implements InputTypeInterface
{

    public function __construct(private readonly string|bool $value)
    {
    }

    public static function createConstraint(): Constraint
    {
        return new AtLeastOneOf([
            new Type('bool'),
            new Regex(
                '/^(1|0|true|false|TRUE|FALSE)$/',
                'This value should match the pattern: ^(1|0|true|false|TRUE|FALSE)$.'
            )
        ]);
    }

    public function toBool(): bool
    {
        if (is_bool($this->value)) {
            return $this->value;
        }
        if ($this->value === 'true') {
            return true;
        }
        if ($this->value === 'false') {
            return false;
        }
        if ($this->value === 'TRUE') {
            return true;
        }
        if ($this->value === 'FALSE') {
            return false;
        }
        if ($this->value === '1') {
            return true;
        }
        if ($this->value === '0') {
            return false;
        }
        throw new \Exception('Value is not bool!');
    }
}
