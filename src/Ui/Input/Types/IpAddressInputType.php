<?php
declare(strict_types=1);

namespace Grifix\Framework\Ui\Input\Types;

use Grifix\Ip\IpAddress;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\Constraints\Ip;

final class IpAddressInputType implements InputTypeInterface
{
    public function __construct(private readonly string $value)
    {
    }

    public static function createConstraint(): Constraint
    {
        return new Ip();
    }

    public function toIpAddress(): IpAddress
    {
        return IpAddress::create($this->value);
    }
}
