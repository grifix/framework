<?php
declare(strict_types=1);

namespace Grifix\Framework\Ui\Input\Types;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\Constraints\AtLeastOneOf;
use Symfony\Component\Validator\Constraints\Regex;
use Symfony\Component\Validator\Constraints\Type;

final class IntInputType implements InputTypeInterface
{


    public function __construct(private readonly string|int $value)
    {
    }

    public static function createConstraint(): Constraint
    {
        return new AtLeastOneOf([
            new Type('int'),
            new Regex(
                '/^[0-9]*$/',
                'This value should match the pattern: ^[0-9]*$.'
            )
        ]);
    }

    public function toInt(): int
    {
        return (int)$this->value;
    }
}
