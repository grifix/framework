<?php
declare(strict_types=1);

namespace Grifix\Framework\Ui\Input\Types;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\Constraints\Positive;

final class PositiveIntType implements InputTypeInterface
{


    public function __construct(private readonly string|int $value)
    {
    }

    public static function createConstraint(): Constraint
    {
        return new Positive();
    }

    public function toInt(): int
    {
        return (int)$this->value;
    }
}
