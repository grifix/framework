<?php
declare(strict_types=1);

namespace Grifix\Framework\Ui\Input\Types;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\Constraints\Type;

final class FloatInputType implements InputTypeInterface
{

    public function __construct(private readonly float $value)
    {
    }

    public static function createConstraint(): Constraint
    {
        return new Type('float');
    }

    public function toFloat(): float
    {
        return $this->value;
    }
}
