<?php
declare(strict_types=1);

namespace Grifix\Framework\Ui\Input\Types;

use Grifix\Uuid\Uuid;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\Constraints;

final class UuidInputType implements InputTypeInterface
{
    public function __construct(private readonly string $value)
    {

    }

    public static function createConstraint(): Constraint
    {
        return new Constraints\Uuid();
    }

    public function toUuid(): Uuid
    {
        return Uuid::createFromString($this->value);
    }
}
