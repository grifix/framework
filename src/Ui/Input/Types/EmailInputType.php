<?php
declare(strict_types=1);

namespace Grifix\Framework\Ui\Input\Types;

use Grifix\Email\Email;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\Constraints\Email as EmailConstraint;

final class EmailInputType implements InputTypeInterface
{


    public function __construct(private readonly string $value)
    {
    }

    public static function createConstraint(): Constraint
    {
        return new EmailConstraint();
    }

    public function toEmail(): Email
    {
        return Email::create($this->value);
    }
}
