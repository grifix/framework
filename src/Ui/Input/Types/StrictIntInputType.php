<?php
declare(strict_types=1);

namespace Grifix\Framework\Ui\Input\Types;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\Constraints\Type;

final class StrictIntInputType implements InputTypeInterface
{

    public function __construct(private readonly int $value)
    {
    }

    public static function createConstraint(): Constraint
    {
        return new Type('int');
    }

    public function toInt(): int
    {
        return $this->value;
    }
}
