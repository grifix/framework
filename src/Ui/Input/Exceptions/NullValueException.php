<?php
declare(strict_types=1);

namespace Grifix\Framework\Ui\Input\Exceptions;

final class NullValueException extends \Exception
{

    public function __construct(string $path)
    {
        parent::__construct(sprintf('Input path [%s] has null value!', $path));
    }
}
