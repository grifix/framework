<?php
declare(strict_types=1);

namespace Grifix\Framework\Ui\Input\Exceptions;

final class UnsupportedInputTypeException extends \Exception
{

    public function __construct(string $inputType)
    {
        parent::__construct(sprintf('Unsupported input type [%s]!', $inputType));
    }
}
