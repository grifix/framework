<?php
declare(strict_types=1);

namespace Grifix\Framework\Ui\Input;

use Symfony\Component\Validator\Validator\ValidatorInterface;

final class InputFactory
{
    public function __construct(private readonly ValidatorInterface $validator)
    {
    }

    /**
     * @template T
     * @param class-string<T> $inputClass
     * @param mixed[] $data
     * @return T
     *
     * @throws InvalidInputException
     */
    public function createInput(string $inputClass, array $data)
    {
        /** @var T $result */
        $result = new $inputClass($this->validator, $data);
        return $result;
    }
}
