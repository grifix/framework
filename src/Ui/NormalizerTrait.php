<?php
declare(strict_types=1);

namespace Grifix\Framework\Ui;

trait NormalizerTrait
{
    protected function normalizeValue(mixed $value): mixed
    {
        if (is_object($value) && method_exists($value, '__toString')) {
            return (string)$value;
        }
        return $value;
    }
}
